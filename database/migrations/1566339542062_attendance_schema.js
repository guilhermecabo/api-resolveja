'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AttendanceSchema extends Schema {
  up () {
    this.create('attendances', (table) => {
      table.increments()
      table.integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
      table.integer('service_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('services')
        .onUpdate('cascade')

      table.string('status').notNullable()
      table.string('endereco').notNullable()
      table.string('desiredTime')
      table.string('finishingTime')
      table.timestamps()
    })
  }

  down () {
    this.drop('attendances')
  }
}

module.exports = AttendanceSchema
