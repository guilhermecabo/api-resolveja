'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderNotificationsSchema extends Schema {
  up () {
    this.create('provider_notifications', (table) => {
      table.increments()
      table.string('content')
      table.boolean('read')
      table.integer('provider_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('providers')
        .onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_notifications')
  }
}

module.exports = ProviderNotificationsSchema
