'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderSchema extends Schema {
  up () {
    this.create('providers', (table) => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('cpf_cnpj', 60).notNullable()
      table.string('uf', 60).notNullable()
      table.string('cidade', 60).notNullable()
      table.string('bairro', 60).notNullable()
      table.string('rua', 60).notNullable()
      table.string('numero', 60).notNullable()
      table.string('type', 20).notNullable()
      table.double('rating').notNullable()
      table.string('image')
      table.timestamps()
    })
  }

  down () {
    this.drop('providers')
  }
}

module.exports = ProviderSchema
