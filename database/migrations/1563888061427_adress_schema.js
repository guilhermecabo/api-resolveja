'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdressSchema extends Schema {
  up () {
    this.create('adresses', (table) => {
      table.increments()
      table.string('cep').notNullable()
      table.string('uf').notNullable()
      table.string('cidade').notNullable()
      table.string('bairro').notNullable()
      table.string('rua').notNullable()
      table.string('numero').notNullable()
      table.string('tipo').notNullable()
      table.string('complemento')
      table.boolean('selected').defaultTo(true).notNullable()
      table.string('latitude')
      table.string('longitude')
      table.integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('adresses')
  }
}

module.exports = AdressSchema
