'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProvidersSchema extends Schema {
  up () {
    this.table('providers', (table) => {
      table.dropUnique('username')
    })
  }

  down () {
    this.table('providers', (table) => {
      table.string('username').unique().alter()
    })
  }
}

module.exports = ProvidersSchema
