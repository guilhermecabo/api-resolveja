'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.alter('users', (table) => {
      table.dropUnique('name')
    })
  }

  down () {
    this.alter('users', (table) => {
      table.string('name', 80).notNullable().unique().alter()
    })
  }
}

module.exports = UsersSchema
