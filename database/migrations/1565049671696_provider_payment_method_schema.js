'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderPaymentMethodSchema extends Schema {
  up () {
    this.create('provider_payment_methods', (table) => {
      table.increments()
      table.timestamps()
      table.integer('provider_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('providers')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.integer('payment_method_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('payment_methods')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down () {
    this.drop('provider_payment_methods')
  }
}

module.exports = ProviderPaymentMethodSchema
