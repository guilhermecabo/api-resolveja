"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.post("users", "UserController.store");
Route.get("user/:id", "UserController.show");
Route.put("user", "UserController.update");

Route.post("login", "SessionController.store");
Route.get("session/user", "SessionController.show").middleware(["auth"]);

Route.post("login/provider", "ProviderSessionController.store");
Route.get("session/provider", "ProviderSessionController.show").middleware([
  "auth:provider"
]);

Route.get("providers/:category?", "ProviderController.index");
Route.get("provider/:id?", "ProviderController.show");
Route.post("providers", "ProviderController.store");
Route.put("providers", "ProviderController.update").middleware([
  "auth:provider"
]);

Route.get("image/provider/:src", "ProviderImageController.show");
Route.post("image/provider", "ProviderImageController.store").middleware([
  "auth:provider"
]);
Route.put("image/provider", "ProviderImageController.update").middleware([
  "auth:provider"
]);

Route.post(
  "category/provider",
  "ProviderByCategoryController.store"
).middleware(["auth:provider"]);

Route.get("service/:id", "ServiceController.show").middleware([
  "auth:provider,jwt"
]);
Route.get("services/:provider", "ServiceController.index").middleware([
  "auth:provider,jwt"
]);
Route.post("services", "ServiceController.store").middleware(["auth:provider"]);
Route.put("services/:id", "ServiceController.update").middleware([
  "auth:provider"
]);

Route.post("attendance", "AttendanceController.store");

Route.get("attendance/user", "UserAttendanceController.index").middleware([
  "auth:jwt"
]);
Route.put("attendance/user/:id", "UserAttendanceController.update").middleware([
  "auth:jwt"
]);

Route.put(
  "attendance/provider/:id",
  "ProviderAttendanceController.update"
).middleware(["auth:provider"]);
Route.put(
  "attendance/provider/:id",
  "ProviderAttendanceController.update"
).middleware(["auth:provider"]);
Route.get(
  "attendances/provider",
  "ProviderAttendanceController.index"
).middleware(["auth:provider"]);
Route.get(
  "attendance/provider/:id",
  "ProviderAttendanceController.show"
).middleware(["auth:provider"]);

Route.post("categories", "CategoryController.store");
Route.get("categories", "CategoryController.index").middleware([
  "auth:jwt,provider"
]);
Route.get("categories/:id", "CategoryController.show");

Route.get("category-image/:src", "CategoryImageController.show");

Route.post("forgot-password", "ForgotPasswordController.store");
Route.put("forgot-password", "ForgotPasswordController.update");

Route.post("address", "AdressController.store").middleware(["auth:jwt"]);

Route.post("payment-method", "PaymentMethodController.store");

Route.get("notifications/user", "UserNotificationController.index").middleware([
  "auth:jwt"
]);
Route.put(
  "notification/user/:id",
  "UserNotificationController.update"
).middleware(["auth:jwt"]);

Route.get(
  "notifications/provider",
  "ProviderNotificationController.index"
).middleware(["auth:provider"]);

Route.put(
  "notification/provider/:id",
  "ProviderNotificationController.update"
).middleware(["auth:provider"]);
