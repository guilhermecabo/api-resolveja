'use strict'

const Adress = use('App/Models/Adress')

class AdressController {
  async store ({ request, response }) {
    const data = request.only([
      'cep',
      'uf',
      'cidade',
      'bairro',
      'rua',
      'numero',
      'tipo',
      'complemento',
      'selected',
      'latitude',
      'longitude',
      'user_id'
    ])

    try {
      const adress = await Adress.create(data)
      return adress
    } catch (err) {
      console.log(err)
      return response
        .status(err.status)
        .send({ err: { message: 'Houve um erro ao cadastrar o endereço' } })
    }
  }

  async update ({ params, request, response }) {
  }

  async destroy ({ params, request, response }) {
  }
}

module.exports = AdressController
