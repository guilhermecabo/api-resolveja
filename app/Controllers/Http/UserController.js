"use strict";

const User = use("App/Models/User");

class UserController {
  async store({ request, response }) {
    try {
      const data = request.only(["name", "password", "email", "cpf", "tel"]);

      const user = await User.create(data);

      return user;
    } catch (err) {
      return response.status(err.status).send({ error: err.message });
    }
  }

  async show({ params, response }) {
    try {
      const user = await User.findOrFail(params.id);
      const address = await user.addresses().first();

      return {
        user: { email: user.email, name: user.name, tel: user.tel },
        address
      };
    } catch (err) {
      return response.status(err.status).send({ error: err.message });
    }
  }

  async update({ request, auth, response }) {
    try {
      const user = await auth.getUser();

      const data = request.only(["name, email, password, cpf, tel"]);

      user.merge(data);

      await user.save();

      return user;
    } catch (err) {
      return response.status(err.status).send({ error: err.message });
    }
  }
}

module.exports = UserController;
