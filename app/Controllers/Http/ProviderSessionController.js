"use strict";

const Provider = use("App/Models/Provider");

class ProviderSessionController {
  async store({ request, auth, response }) {
    try {
      const { email, password } = request.all();

      const token = await auth
        .authenticator("provider")
        .attempt(email, password);

      let user = await Provider.findBy("email", email);

      user = user.toJSON();

      delete user["password"];
      delete user[""]
      delete user["password"];

      return { token, user };
    } catch (err) {
      return response
        .status(err.status)
        .send({ error: true, message: err.message });
    }
  }

  async show({ request, auth, response }) {
    try {
      const provider = await auth.authenticator("provider").getUser();
      return provider;
    } catch (err) {
      return response.status(err.status).send({
        err: { message: "Houve um erro ao carregar os dados do prestador" }
      });
    }
  }
}

module.exports = ProviderSessionController;
