'use strict'

const Pivot = use('App/Models/ProviderPaymentMethod')
const PaymentMethod = use('App/Models/PaymentMethod')

class PaymentMethodController {
  async index ({ request, response }) {
    const { provider } = request

    if (provider) {
      const pivots = await Pivot.select('provider_id', provider).fetch()
      const methods = []
      pivots.map((pivot) => {
        const method = PaymentMethod.findOrFail(pivot.payment_method_id)
        methods.push(method)
      })

      return methods
    }

    if (!provider) {
      const methods = await PaymentMethod.all()

      return methods
    }
  }

  async store ({ request, response }) {
    const data = request.only('nome')
    const paymentMethod = await PaymentMethod.create(data)

    return paymentMethod
  }

  async show ({ params, request, response }) {
  }

  async update ({ params, request, response }) {
  }

  async destroy ({ params, request, response }) {
  }
}

module.exports = PaymentMethodController
