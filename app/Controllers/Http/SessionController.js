"use strict";

const User = use("App/Models/User");

class SessionController {
  async store({ request, auth, response }) {
    try {
      const { email, password } = request.all();

      const token = await auth.attempt(email, password);

      let user = await User.findBy(`email`, email);

      user = user.toJSON();

      delete user["password"];
      delete user[""];
      delete user["password"];

      return { token, user };
    } catch (err) {
      return response.status(401).send({ error: err.message });
    }
  }

  async show({ auth, response }) {
    try {
      let user = await auth.getUser()
      const addresses = await user.addresses().fetch()
      
      user = user.toJSON();

      delete user["password"];
      delete user[""];
      delete user["password"];

      return { user, addresses }

    } catch (err) {
      return response.status(err.status).send({
        // err: { message: "Houve um erro ao carregar os dados do usuário" }
        err: { message: err.message }
      });
    }
  }
}

module.exports = SessionController;
