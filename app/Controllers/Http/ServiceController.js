"use strict";

const Service = use("App/Models/Service");

/**
 * Resourceful controller for interacting with services
 */
class ServiceController {
  async index({ params, response }) {
    try {
      const services = await Service.query()
        .where("provider_id", params.provider)
        .fetch();
      return services;
    } catch (err) {
      return response
        .status(err.status)
        .send({ err: { message: "Houve um erro na busca por serviços" } });
    }
  }

  async store({ request, response, auth }) {
    const provider = await auth.authenticator("provider").getUser();

    const data = request.only(["name", "description", "avg_price"]);

    try {
      const service = await Service.create({
        ...data,
        provider_id: provider.id
      });

      return service;
    } catch (err) {
      return response.status(err.status).send({
        err: { message: "Houve um erro ao cadastrar o novo serviço" }
      });
    }
  }

  /**
   * Display a single service.
   * GET services/:id
   */
  async show({ params, response }) {
    try {
      const service = await Service.findOrFail(params.id);
      const provider = await service.provider().fetch();

      return {
        service,
        provider: { username: provider.username, email: provider.email }
      };
    } catch (err) {
      return response.status(err.status).send({
        err: { message: "Houve um erro na consulta de dados do serviço" }
      });
    }
  }

  /**
   * Update service details.
   * PUT or PATCH services/:id
   */
  async update({ params, request, response, auth }) {
    const service = await Service.find(params.id);

    const provider = await auth.authenticator("provider").getUser();

    if (provider.id !== service.provider_id) {
      return response
        .status(401)
        .send({ error: "Esse serviço não te pertençe" });
    }

    const data = request.only(["name", "description", "avg_price"]);

    service.merge(data);

    await service.save();

    return service;
  }

  /**
   * Delete a service with id.
   * DELETE services/:id
   */
  async destroy({ params, request, response }) {}
}

module.exports = ServiceController;
