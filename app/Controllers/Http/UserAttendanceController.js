"use strict";

const Attendance = use("App/Models/Attendance");

/**
 * Resourceful controller for interacting with userattendances
 */
class UserAttendanceController {
  /**
   * Show a list of all userattendances.
   * GET userattendances
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ auth, response }) {
    try {
      const user = await auth.getUser();

      let attendances = Object.values(
        (await user
          .attendances()
          .orderBy("created_at", "desc")
          .fetch()).toJSON()
      );

      const data = Promise.all(
        attendances.map(async attendance => {
          const att = await Attendance.find(attendance.id);
          const service = await att.service().fetch();
          const { username, image } = await service.provider().fetch();

          return {
            ...attendance,
            service,
            provider: { username, image }
          };
        })
      );

      return data;
    } catch (err) {
      return response.status(err.status).send({ error: err.message });
    }
  }

  /**
   * Render a form to be used for creating a new userattendance.
   * GET userattendances/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new userattendance.
   * POST userattendances
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {}

  /**
   * Display a single userattendance.
   * GET userattendances/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing userattendance.
   * GET userattendances/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update userattendance details.
   * PUT or PATCH userattendances/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a userattendance with id.
   * DELETE userattendances/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = UserAttendanceController;
