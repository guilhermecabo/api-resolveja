"use strict";

const Notification = use("App/Models/UserNotification");

class UserNotificationController {
  async index({ auth, response }) {
    try {
      const user = await auth.getUser();

      const notifications = await Notification.query()
        .where("user_id", user.id)
        .orderBy("created_at", "desc")
        .fetch();

      return notifications;
    } catch (err) {
      return response.status(err.status).send(err.message);
    }
  }

  async update({ params, response }) {
    try {
      const notification = await Notification.findOrFail(params.id);
      notification.merge({ read: true });
      await notification.save()
    } catch (err) {
      return response.status(err.status).send(err.message);
    }
  }
}

module.exports = UserNotificationController;
