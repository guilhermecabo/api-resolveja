'use strict';

const Helpers = use('Helpers')
const crypto = require('crypto')

class ProviderImageController {
  async show ({ params, response }) {
    return response.download(
      await Helpers.tmpPath(`/providers_images/${params.src}`)
    )
  }

  async store ({ auth, request, response }) {
    try {
      const provider = await auth.authenticator('provider').getUser()

      const upload = request.file('image', { size: '15mb' })

      const file = `${crypto.randomBytes(20).toString('hex')}.${
        upload.subtype
      }`

      await upload.move(Helpers.tmpPath('providers_images'), {
        name: file
      })

      if (!upload.move()) {
        return { error: upload.error() }
      }

      const image = file

      provider.merge({ image })
      await provider.save()

      return { image }
    } catch (err) {
      return response.status(err.status).send({ errp: err.message })
    }
  }

  async update ({ auth, request, response }) {
    try {
      const provider = await auth.authenticator('provider').getUser()

      const upload = request.file('image', { size: '15mb' })

      const file = `${crypto.randomBytes(20).toString('hex')}.${
        upload.subtype
      }`

      await upload.move(Helpers.tmpPath('providers_images'), {
        name: file
      })

      if (!upload.move()) {
        throw upload.error()
      }

      const image = file

      provider.merge({ image })
      await provider.save()

      return image
    } catch (err) {
      return response.status(err.status).send(err.message)
    }
  }
}

module.exports = ProviderImageController
