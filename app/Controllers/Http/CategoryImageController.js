'use strict'

const Helpers = use('Helpers')

class CategoryImageController {
  show ({ params, response }) {
    return response.download(Helpers.tmpPath(`/categories_images/${params.src}`))
  }
}

module.exports = CategoryImageController
