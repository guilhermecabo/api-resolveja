'use strict';

const Attendance = use('App/Models/Attendance')
const Service = use('App/Models/Service')
const Provider = use('App/Models/Provider')
const UserNotification = use('App/Models/UserNotification')
const ProviderNotification = use('App/Models/ProviderNotification')

class AttendanceController {
  async store ({ request, response, auth }) {
    try {
      const data = request.only(['service_id', 'desiredTime', 'endereco'])

      const user = await auth.getUser()

      let endereco = '';

      if (!data.endereco) {
        const {
          rua,
          numero,
          bairro,
          cidade,
          uf
        } = await user.addresses().first()

        endereco = `${rua}, ${numero}, ${bairro} - ${cidade} / ${uf}`
      } else {
        endereco = data.endereco
      }

      const { service_id, desiredTime } = data

      const { id } = user

      const status = 'Aguardando Confirmação';

      const attendance = await Attendance.create({
        service_id,
        desiredTime,
        endereco,
        user_id: id,
        status
      })

      const service = await Service.find(service_id)

      const provider = await Provider.find(service.provider_id)

      await UserNotification.create({
        content: `Você solicitou a contratação de um novo serviço de ${provider.username}`,
        user_id: id,
        read: false
      })

      try {
        await ProviderNotification.create({
          content: `Você foi solicitado para um novo serviço por ${user.name}`,
          provider_id: provider.id,
          read: false
        })
      } catch (err) {
        return err.message
      }

      return attendance
    } catch (err) {
      return response.status(err.status).send({ error: err.message })
    }
  }

  async show ({ params, request, response, view }) {}

  async destroy ({ params, request, response }) {}
}

module.exports = AttendanceController
