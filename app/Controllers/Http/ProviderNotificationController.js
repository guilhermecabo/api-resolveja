"use strict";

const Provider = use("App/Models/Provider");
const Notification = use("App/Models/ProviderNotification");

class ProviderNotificationController {
  async index({ auth, response }) {
    try {
      const provider = await auth.authenticator("provider").getUser();

      const notifications = await Notification.query()
        .where("provider_id", provider.id)
        .orderBy("created_at", "desc")
        .fetch();

      return notifications;
    } catch (err) {
      return response.status(err.status).send(err.message);
    }
  }

  async update({ params, response }) {
    try {
      const notification = await Notification.findOrFail(params.id);
      notification.merge({ read: true });
      await notification.save()
      return notification;
    } catch (err) {
      return response.status(err.status).send(err.message);
    }
  }
}

module.exports = ProviderNotificationController;
