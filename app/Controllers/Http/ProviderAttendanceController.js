"use strict";

const Attendance = use("App/Models/Attendance");
/**
 * Resourceful controller for interacting with providerattendances
 */
class ProviderAttendanceController {
  async index({ auth, response }) {
    try {
      const provider = await auth.authenticator("provider").getUser();

      let attendances = Object.values(
        (await provider
          .attendances()
          .where("status", "<>", "Finalizado")
          .where("status", "<>", "Cancelado")
          .orderBy("created_at", "asc")
          .fetch()).toJSON()
      );

      const data = Promise.all(
        attendances.map(async attendance => {
          const att = await Attendance.find(attendance.id);
          const { name, email, tel } = await att.user().fetch();
          const service = await att.service().fetch();

          return {
            ...attendance,
            service,
            user: { name, email, tel }
          };
        })
      );

      return data;
    } catch (err) {
      return response.status(err.status).send(err.message);
    }
  }

  async create({ request, response, view }) {}

  async store({ request, response }) {}

  async show({ auth, params, response }) {
    const provider = await auth.authenticator("provider").getUser();
    const attendance = await Attendance.find(params.id);
    const service = await attendance.service().fetch();

    const checkProvider = await service.provider().fetch();

    if (checkProvider.id !== provider.id) {
      return response
        .status(401)
        .send({ error: "Essa solicitação não te pertence" });
    }

    const { name, email, tel } = await attendance.user().fetch();

    return { ...attendance.toJSON(), user: { name, email, tel }, service };
  }

  async update({ auth, params, request, response }) {
    const provider = await auth.authenticator("provider").getUser();
    const attendance = await Attendance.find(params.id);
    const service = await attendance.service().fetch();

    const checkProvider = await service.provider().fetch();

    if (checkProvider.id !== provider.id) {
      return response
        .status(401)
        .send({ error: "Essa solicitação não te pertence" });
    }

    const { status } = request.all();

    const allowedStatus = [
      "Confirmado",
      "Em atendimento",
      "Finalizado",
      "Cancelado"
    ];

    if (!allowedStatus.includes(status)) {
      return response.status(401).send({ error: "Status inválido" });
    }

    attendance.merge({ status });
    await attendance.save();

    return attendance;
  }

  async edit({ params, request, response, view }) {}

  async destroy({ params, request, response }) {}
}

module.exports = ProviderAttendanceController;
