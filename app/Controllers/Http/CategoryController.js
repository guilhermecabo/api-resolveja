"use strict";

const Category = use("App/Models/Category");
const crypto = require("crypto");
const Helpers = use("Helpers");

/**
 * Resourceful controller for interacting with categories
 */
class CategoryController {
  /**
   * Show a list of all categories.
   * GET categories
   */
  async index({ request, response }) {
    try {
      const categories = await Category.all();
      return categories;
    } catch (err) {
      return response.status(err.status).send({
        err: { message: "Ocorreu um erro ao consultar as categorias" }
      });
    }
  }

  /**
   * Create/save a new category.
   * POST categories
   */
  async store({ request, response }) {
    try {
      const data = request.only(["name"]);

      try {
        const allowed = ["jpg", "jpeg", "png"];

        const upload = request.file("image", { size: "15mb" });

        if (!allowed.includes(upload.subtype)) throw upload.error();

        const file = `${crypto.randomBytes(20).toString("hex")}.${
          upload.subtype
        }`;

        await upload.move(Helpers.tmpPath("categories_images"), {
          name: file
        });

        if (!upload.move()) {
          throw upload.error();
        }

        data.image = file;
      } catch (err) {
        throw err;
      }

      const category = await Category.create(data);

      return category;
    } catch (err) {
      return response
        .status(err.status)
        .send({ err: { message: "Ocorreu um erro ao cadastras a categoria" } });
    }
  }

  /**
   * Display a single category.
   * GET categories/:id
   */
  async show({ params, request, response }) {
    const category = await Category.find(params.id);

    return category;
  }

  /**
   * Update category details.
   * PUT or PATCH categories/:id
   */
  async update({ params, request, response }) {}

  /**
   * Delete a category with id.
   * DELETE categories/:id
   */
  async destroy({ params, request, response }) {}
}

module.exports = CategoryController;
