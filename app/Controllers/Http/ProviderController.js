"use strict";

const Provider = use("App/Models/Provider");
const Category = use("App/Models/Category");
const Service = use("App/Models/Service");
const ProviderCategory = use("App/Models/ProviderCategory");
const Helpers = use("Helpers");
const Drive = use("Drive");

class ProviderController {
  async index({ params }) {
    if (params.category) {
      const category = await Category.find(params.category);
      const providers = Object.values(
        (await category.providers().fetch()).toJSON()
      );

      const data = Promise.all(
        providers.map(async provider => {
          return { ...provider, category };
        })
      );

      return data;
    }

    const providers = Object.values((await Provider.all()).toJSON());

    const data = Promise.all(
      providers.map(async provider => {
        const prv = await Provider.find(provider.id);

        const category = (await prv.categories().fetch()).toJSON();

        return { ...provider, category };
      })
    );

    return data;
  }

  async store({ request, response }) {
    try {
      const data = request.only([
        "username",
        "password",
        "email",
        "cpf_cnpj",
        "uf",
        "cidade",
        "bairro",
        "rua",
        "numero",
        "type",
        "rating"
      ]);

      if (await Provider.findBy("email", data.email)) {
        return response
          .status(401)
          .send({ error: true, message: "E-mail em uso" });
      }

      if (await Provider.findBy("cpf_cnpj", data.cpf_cnpj)) {
        return response
          .status(401)
          .send({ error: true, message: "CPF/CNPJ em uso" });
      }

      const provider = await Provider.create(data);

      await ProviderCategory.create({
        provider_id: provider.id,
        category_id: request.input("category_id")
      });

      return provider;
    } catch (err) {
      return response
        .status(err.status)
        .send({ error: true, message: err.message });
    }
  }

  async show({ params, response }) {
    try {
      const provider = await Provider.findOrFail(params.id);
      const services = await Service.query()
        .where("provider_id", params.id)
        .fetch();
      return { provider, services };
    } catch (err) {
      return response.status(err.status).send({
        err: { message: "Houve um erro ao consultar os dados do prestador" }
      });
    }
  }

  async update({ auth, request, response }) {
    const { email, oldPassword, cpf_cnpj } = request.all();

    let provider = await auth.authenticator("provider").getUser();

    if (email !== provider.email) {
      if (await Provider.findBy("email", email)) {
        return response
          .status(401)
          .send({ error: true, message: "E-mail em uso" });
      }
    }

    if (cpf_cnpj !== provider.cpf_cnpj) {
      if (await Provider.findBy("cpf_cnpj", cpf_cnpj)) {
        return response
          .status(401)
          .send({ error: true, message: "CPF/CNPJ em uso" });
      }
    }

    if (oldPassword) {
      try {
        await auth.authenticator("provider").attempt(email, oldPassword);
      } catch (err) {
        return response.status(401).send(err.message);
      }
    }

    delete request.all()["oldPassword"];

    provider.merge(request.all());
    await provider.save();

    provider = provider.toJSON();

    delete provider["password"];

    return provider;
  }
}

module.exports = ProviderController;
