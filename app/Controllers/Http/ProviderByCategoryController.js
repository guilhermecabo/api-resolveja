'use strict'

const ProviderCategory = use('App/Models/ProviderCategory')
const Provider = use('App/Models/Provider')

class ProviderByCategoryController {
  async index ({ params, response }) {
    const providers = Provider
      .query()
      .where('id', params.category)
      .fetch()

    return providers
  }

  async store ({ request, response }) {
    const data = request.only([
      'provider_id',
      'category_id'
    ])

    const providerCategory = await ProviderCategory.create(data)

    return providerCategory
  }
}

module.exports = ProviderByCategoryController
