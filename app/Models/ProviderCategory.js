'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProviderCategory extends Model {
  static get table () {
    return 'providers_categories'
  }

  async provider () {
    return this.belongsTo('App/Models/Provider')
  }

  async category () {
    return this.belongsTo('App/Models/Category')
  }
}

module.exports = ProviderCategory
