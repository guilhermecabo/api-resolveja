'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Category extends Model {
  providers () {
    return this.belongsToMany('App/Models/Provider')
      .pivotModel('App/Models/ProviderCategory')
  }
}

module.exports = Category
