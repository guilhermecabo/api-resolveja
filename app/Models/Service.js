'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const Model = use('Model')

class Service extends Model {
  provider () {
    return this.belongsTo('App/Models/Provider')
  }

  attendances () {
    return this.hasMany('App/Models/Attendance')
  }
}

module.exports = Service
